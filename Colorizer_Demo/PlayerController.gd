extends KinematicBody

var gravity = -9.8
var velocity = Vector3()
var camera
var character

const SPEED = 6
const ACCELERATION = 3
const DE_ACCELERATION = 5

func _ready():
	camera = get_node("../Camera").get_global_transform()
	character = get_node(".")

func _physics_process(delta):
	var dir = Vector3()

	if(Input.is_action_pressed("move_fw")):
		dir += -camera.basis[2]

	if(Input.is_action_pressed("move_bw")):
		dir += camera.basis[2]

	if(Input.is_action_pressed("move_l")):
		dir += -camera.basis[0]

	if(Input.is_action_pressed("move_r")):
		dir += camera.basis[0]

	dir.y = 0
	dir = dir.normalized()

	velocity.y += delta * gravity

	var hv = velocity
	hv.y = 0

	var new_pos = dir * SPEED
	var accel = DE_ACCELERATION

	if (dir.dot(hv) > 0):
		accel = ACCELERATION

	hv = hv.linear_interpolate(new_pos, accel * delta)

	velocity.x = hv.x
	velocity.z = hv.z

	velocity = move_and_slide(velocity, Vector3(0,1,0))
	
	if velocity.x > 0 or velocity.y > 0:
		var angle = atan2(hv.x, hv.z)
		var char_rot = character.get_rotation()
		char_rot.y = angle
		character.set_rotation(char_rot)
