tool
extends TextureButton

export var url = "https://formation-facile.fr/"

func _enter_tree():
	connect("pressed", self, "clicked")

func clicked():
    OS.shell_open(url)