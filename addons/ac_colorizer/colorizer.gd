tool
extends EditorPlugin

var dock
var version = "1.0.0"

func _enter_tree():
    dock = preload("res://addons/ac_colorizer/my_dock.tscn").instance()
    add_control_to_dock(DOCK_SLOT_LEFT_UL, dock)

func _exit_tree():
    remove_control_from_docks(dock)
    dock.free()
