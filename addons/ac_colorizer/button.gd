tool
extends Button

export var color = Color(1,0,0)
var edi

func _init():
    var plugin = EditorPlugin.new()
    edi = plugin.get_editor_interface().get_selection()

func _enter_tree():
	connect("pressed", self, "clicked")

func clicked():
    var selected = edi.get_selected_nodes() 
    var material = SpatialMaterial.new()
    material.albedo_color = color
    #material.emission = true
    if not selected.empty():
        for item in selected:
            item.set_surface_material(0, material)
            # pass
