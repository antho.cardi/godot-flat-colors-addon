# Godot-flat-colors-addon

Un plugin pour Godot permettant de colorer rapidement la géométrie en 1 clic !
Il s'agit d'une palette de couleurs flat que j'utilise et qui correspondent parfaitement au style low poly / cartoon.
Pas besoin de perdre du temps à trouver de belles couleurs ou à créer des materials, le plugin le fait en 1 clic !
Développé sous Godot 3.1 64 bit.

## Installation

* Téléchargez l'addon (addons/ac_colorizer) et placez le dossier dans votre projet Godot. 
* Activez l'addon via le menu "Projet/Paramètres du projet" onglet "Extenions"
* Un nouveau dock apparaît sur la gauche, vous pouvez utiliser l'addon.

![Flat colors Godot addon](http://www.anthony-cardinale.fr/_public/ac_colorizer/Plugin-godot-flat-colorizer.png "Plugin pour colorer sous Godot")

## Scène d'exemple / Démo

J'ai créé une démo vous permettant de tester rapidement le plugin. Si vous voulez récupérer cette démo, téléchargez et importez le dossier "Colorizer_Demo".
Dans ce dossier vous retrouverez une scène sans couleur (BasicLevel) ainsi que la même scène colorée (BasicLevelColored).
Cela vous permettra de tester le plugin :

![Palette couleurs Godot addon](http://www.anthony-cardinale.fr/_public/ac_colorizer/Godot-color-picker-flat-design-addon.png "Plugin palette couleurs Godot")

Voici le résultat final de la scène après coloration :

![Niveau coloré sous Godot](http://www.anthony-cardinale.fr/_public/ac_colorizer/Scene-exemple.png "projet Godot en couleurs flat")

## Suggestions

Pensez à me faire parvenir vos commentaires et demandes afin que je puisse améliorer l'addon.
